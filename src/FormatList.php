<?php
namespace Fulcrum\Images;

use ArrayAccess;
use Fulcrum\Images\Exception\InvalidRepositoryException;
use Fulcrum\Images\Format\AbstractFormat;

class FormatList implements ArrayAccess, \Iterator, \Countable{

    /** @var AbstractFormat[] */
    protected $items = [];
    protected $position = 0;

    public function offsetExists($offset)
    {
        foreach ($this->items as $item) {
            if ($item->name() == $offset) {
                return true;
            }
        }
       return false;
    }

    public function offsetGet($offset)
    {
        foreach ($this->items as $item) {
            if ($item->name() == $offset) {
                return $item;
            }
        }
        return null;
    }

    public function offsetSet($offset, $value)
    {
       if (is_null($offset)) {
           $this->items[] = $value;
       }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function current()
    {
        return $this->items[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function toArray(){
        return $this->items;
    }

    public function count()
    {
        return count($this->container);
    }
}
