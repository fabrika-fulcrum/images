<?php
namespace Fulcrum\Images\Preop;

use Fulcrum\Images\Image;
use Imagick;

class DetectFeatures implements PreopInterface {

    const METADATA_KEY = 'features';

    protected $size = 50;
    protected $blur = 5;

    public function size($size) {
        $this->size = $size;
        return $this;
    }

    public function blur($blur) {
        $this->blur = $blur;
        return $this;
    }

    public function process(Image $image)
    {
        $pixels = (new \Images\Analysis\DetectFeatures())->calculateFeaturesMap($image->getLoader()->source,$this->size,$this->blur);
        $image->setMetadata(static::METADATA_KEY, $pixels);
    }

}
