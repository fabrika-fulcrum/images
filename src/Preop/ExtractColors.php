<?php
namespace Fulcrum\Images\Preop;

use ColorThief\ColorThief;
use Fulcrum\Images\Image;
use Imagick;

class ExtractColors implements PreopInterface {

    const METADATA_KEY = 'colors';

    protected $size = 0;

    public function size($size) {
        $this->size = $size;
        return $this;
    }

    public function process(Image $image)
    {
        $colors = (new \Images\Analysis\ExtractColors())->extractColors($image->getLoader()->source, $this->size);
        $image->setMetadata(static::METADATA_KEY, $colors);
    }

}
