<?php
namespace Fulcrum\Images\Preop;

use Fulcrum\Images\Image;

interface PreopInterface {
    public function process(Image $image);
}
