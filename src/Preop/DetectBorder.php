<?php
namespace Fulcrum\Images\Preop;

use Fulcrum\Images\Image;
use Imagick;

class DetectBorder implements PreopInterface {

    const METADATA_KEY = 'border_color';

    protected $size = 0;

    public function size($size) {
        $this->size = $size;
        return $this;
    }

    public function process(Image $image)
    {
        $result = (new \Images\Analysis\DetectBorder())->analyzeBorder($image->getLoader()->source,$this->size);
        $image->setMetadata(static::METADATA_KEY, $result);
    }

}
