<?php
namespace Fulcrum\Images;

class Images {
    /**
     * @var ImageManager
     */
    protected static $manager;

    public static function getManager(){
        if (static::$manager === null) {
            static::$manager = new ImageManager();
        }
        return static::$manager;
    }

    public static function setDefaults(ImagesConfig $imagesConfig) {
        static::getManager()->setConfig($imagesConfig);
    }

    public static function registerRepository($name) {
        return static::getManager()->registerRepository($name);
    }
    
    public static function getRepository($name) {
        return static::$manager->getRepository($name);
    }

    public static function createImage($src) {
        return static::getManager()->createImage($src);
    }

    public static function findByUrl($url) {
        return static::getManager()->findByUrl($url);
    }

    public static function findFormatByUrl($url) {
        return static::getManager()->findFormatByUrl($url);
    }

    public static function find($id) {
        return static::getManager()->find($id);
    }



}
