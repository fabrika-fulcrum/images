<?php
namespace Fulcrum\Images;

use Fulcrum\Images\Exception\InvalidRepositoryException;

class RepositoryList implements \ArrayAccess {

    /** @var Repository[] */
    protected $items = [];

    public function offsetExists($offset)
    {
       return isset($this->items[$offset]);
    }

    public function offsetGet($offset)
    {
        foreach ($this->items as $item) {
            if ($item->name() == $offset) {
                return $item;
            }
        }
        return null;
    }

    public function offsetSet($offset, $value)
    {
       if ($offset === null) {
           $this->items[$value->name()] = $value;
       } else {
           $this->items[$offset] = $value;
       }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function getDefault(){
        foreach ($this->items as $item) {
            if ($item->isDefault()) {
                return $item;
            }
        }
        if (count($this->items) > 0) {
            return $this->items[0];
        }
        throw new InvalidRepositoryException('No repositories exist in image manager');
    }
    
}
