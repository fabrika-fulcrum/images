<?php
namespace Fulcrum\Images;

use Fulcrum\Enum\Enum;

class ImageType extends Enum {
    const PNG = "png";
    const JPG = "jpg";
    const GIF = "gif";
    const SVG = "svg";
    
    public static function getTypeByExtension($extension) {
        //_d($extension,'ImageType::getTypeByExtension');
        $types = [
            'jpg' => new ImageType(ImageType::JPG),
            'png' => new ImageType(ImageType::PNG),
            'gif' => new ImageType(ImageType::GIF),
            'svg' => new ImageType(ImageType::SVG),
        ];
        $requestedType = strtolower($extension);
        if (array_key_exists($requestedType,$types)) {
            return $types[$requestedType];
        } else {
            return null;
        }
    }

    public static function getTypeByMimeType($mimeType) {
        //_d($mimeType,'ImageType::getTypeByMimeType');
        $types = [
            'image/jpeg' => new ImageType(ImageType::JPG),
            'image/png' => new ImageType(ImageType::PNG),
            'image/gif' => new ImageType(ImageType::GIF),
            'image/svg+xml' => new ImageType(ImageType::SVG),
        ];
        $requestedType = strtolower($mimeType);
        if (array_key_exists($requestedType,$types)) {
            return $types[$requestedType];
        } else {
            return null;
        }
    }
    
    public static function listExtensions(){
        return ['jpg','png','gif','svg'];
    }

    public function is($type) {
        return strtolower($type) == $this->value;
    }
}
