<?php
namespace Fulcrum\Images\StorageMethod;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageType;
use Fulcrum\Images\Repository;
use Fulcrum\Images\Utils\Strings;

abstract class AbstractStorageMethod
{

    /**
     * @return Path
     * @throws \Filesystem\Exception\InvalidPathOperationException
     */
    protected function getSourceDir()
    {
        return $this->getRepository()->getSourceDir();
    }

    /**
     * @var Repository
     */
    protected $repository;

    public function setRepository(Repository $repository)
    {
        $this->repository = $repository;
        return $this;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    protected function generateID()
    {
        return Strings::random(40,'0123456789abcdefghijklmnopqrstuvwxyz');
    }

    abstract public function findByUrl($url);

    abstract public function findById($id);

    /**
     * @param Image $image
     * @return Path
     */
    abstract public function getNewNameForImage(Image $image);

    protected function iterateExtensions(Path $basePath){
        $types = ImageType::listExtensions();
        foreach ($types as $type) {
            if ($basePath->extension($type)->exists()) {
                return $basePath->extension($type);
            }
        }
        return false;
    }

    
}
