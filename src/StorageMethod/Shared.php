<?php
namespace Fulcrum\Images\StorageMethod;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Image;
use Fulcrum\Images\Repository;
use Fulcrum\Images\ServeStrategy;

class Shared extends AbstractStorageMethod
{
    protected function getOutputDir(){
        return Path::CreateDir($this->getRepository()->getManager()->getConfig()->getOutputPath())
            ->appendDir($this->getRepository()->name());
    }

    public function findByUrl_($url)
    {
        $parts = explode('/', $url);
        $sourceFile = $this->getSourceDir()->append($parts[1]);

        $image = new Image();
        $image->setRepository($this->getRepository());
        $image->setLoader($sourceFile);
        return $image;
    }

    public function findByUrl($url)
    {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getRepository()->getManager()->getConfig()->getPublicPath());
        $id = $path->filename();
        return $this->findById($id);
    }
    
    public function getNewNameForImage(Image $image)
    {
        return $this->generateID().'.'.$image->getType();
        //return $this->getSourceDir()->append($this->generateID())->extension($image->getType());
    }

    public function findById($id, $ownerId=null){
        $basePath = $this->getSourceDir()->append($id);
        $existingPath = $this->iterateExtensions($basePath);
        if ($existingPath) {
            return $existingPath;
        }
        throw new ImagesException('Could not find image '.$id);
    }
}
