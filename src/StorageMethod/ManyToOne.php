<?php
namespace Fulcrum\Images\StorageMethod;

use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Exception\IncompleteInformationException;
use Fulcrum\Images\Image;
use Fulcrum\Images\Repository;

class ManyToOne extends AbstractStorageMethod {

    public function findByUrl($url)
    {
        $parts = explode('/',$url);
    }


    public function getNewNameForImage(Image $image)
    {
        if (!$image->hasParent()) {
            throw new IncompleteInformationException('Image needs to belong to a parent record before being stored in a one to many repository');
        }
        return $image->getParentId().'/'.$this->generateID().'.'.$image->getType();
    }

    public function findById($id, $ownerId=null){
        if ($ownerId === null) {
            throw new IncompleteInformationException('Belonging record not specified for One to One relationship');
        }
        $types = ['jpg','png','svg','gif'];
        $basePath = $this->getSourceDir()->appendDir($ownerId)->filename($id);
        foreach ($types as $type) {
            if ($basePath->extension($type)->exists()) {
                return $basePath->extension($type);
            }
        }
        throw new ImagesException('Could not find image '.$id);
    }

}
