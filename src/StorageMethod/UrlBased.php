<?php
namespace Fulcrum\Images\StorageMethod;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Image;
use Fulcrum\Images\Repository;
use Fulcrum\Images\Utils\RegexMatcher;
use Fulcrum\Images\Utils\Strings;

class UrlBased extends AbstractStorageMethod
{
    protected $format;
    protected $constraints;
    protected $nameLength = 40;
    protected $nameChars = '0123456789abcdefghijklmnopqrstuvwxyz';

    public function format($format){
        $this->format = $format;
        return $this;
    }

    public function constraints($constraints) {
        $this->constraints = $constraints;
        return $this;
    }

    public function nameLength($length) {
        $this->nameLength = $length;
        return $this;
    }

    public function nameChars($chars) {
        $this->nameChars = $chars;
        return $this;
    }

    protected function getOutputDir(){
        return Path::CreateDir($this->getRepository()->getManager()->getConfig()->getOutputPath(),$this->getRepository()->name());
    }

    public function getNewNameForImage(Image $image)
    {
        return $this->generateID().'.'.$image->getType();
        //return $this->getSourceDir()->append($this->generateID())->extension($image->getType());
    }

    function random_str($length=20, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyz')
    {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    protected function generateID()
    {
        return Strings::random($this->nameLength, $this->nameChars);
    }

    public function findByUrl($url)
    {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getRepository()->getManager()->getConfig()->getPublicPath());

        $matcher = new RegexMatcher($this->format, $this->constraints, $path->segment(-1));
        $id = $matcher->get('id');
        return $this->findById($id);
    }

    public function findById($id){
        $basePath = $this->getSourceDir()->append($id);
        $existingPath = $this->iterateExtensions($basePath);
        if ($existingPath) {
            return $existingPath;
        }
        throw new ImagesException('Could not find image '.$id);
    }
}
