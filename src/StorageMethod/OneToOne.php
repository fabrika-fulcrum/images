<?php
namespace Fulcrum\Images\StorageMethod;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Exception\IncompleteInformationException;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageType;
use Fulcrum\Images\Repository;

class OneToOne extends AbstractStorageMethod {

    public function findByUrl($url)
    {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getRepository()->getManager()->getConfig()->getPublicPath());
        $id = $path->filename();
        return $this->findById($id);
    }

    public function getNewNameForImage(Image $image)
    {
        if (!$image->hasParent()) {
            throw new IncompleteInformationException('Image needs to belong to a parent record before being stored in a one to one repository');
        }
        return $image->getParentId().'.'.$image->getType();
    }

    public function findById($id, $ownerId=null){
        $basePath = $this->getSourceDir()->filename($id);
        $existingPath = $this->iterateExtensions($basePath);
        if ($existingPath) {
            return $existingPath;
        }
        throw new ImagesException('Could not find image '.$id);
    }

}
