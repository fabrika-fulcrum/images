<?php
namespace Fulcrum\Images\Geometry;

use Fulcrum\Images\Exception\ColorException;

class Color implements \JsonSerializable{
    const COMPONENT_RED = 'r';
    const COMPSNENT_GREEN = 'g';
    const COMPONENT_BLUE = 'b';
    const COMPONENT_ALPHA = 'a';

    protected $red;
    protected $green;
    protected $blue;
    protected $alpha = 1;

    public static function CreateFromImagickPixel(\ImagickPixel $px){
        return new Color(
          $px->getColorValue(\Imagick::COLOR_RED),
          $px->getColorValue(\Imagick::COLOR_GREEN),
          $px->getColorValue(\Imagick::COLOR_BLUE),
          $px->getColorValue(\Imagick::COLOR_ALPHA)
        );
    }

    public static function CreateFromHex($hexColor) {
        if (substr($hexColor,0,1) == '#') {
            $hexColor = substr($hexColor,1);
        }
        $r = 0; $g=0; $b=0; $a=255;
        if (strlen($hexColor) == 6) {
            $r = hexdec(substr($hexColor,0,2));
            $g = hexdec(substr($hexColor,2,2));
            $b = hexdec(substr($hexColor,4,2));
        } else if (strlen($hexColor) == 3) {
            $r = hexdec(substr($hexColor,0,1).substr($hexColor,0,1));
            $g = hexdec(substr($hexColor,1,1).substr($hexColor,1,1));
            $b = hexdec(substr($hexColor,2,1).substr($hexColor,2,1));
        } else if (strlen($hexColor) == 8) {
            $r = hexdec(substr($hexColor,0,2));
            $g = hexdec(substr($hexColor,2,2));
            $b = hexdec(substr($hexColor,4,2));
            $a = hexdec(substr($hexColor,6,2));
        } else if (strlen($hexColor) == 4){
            $r = hexdec(substr($hexColor,0,1).substr($hexColor,0,1));
            $g = hexdec(substr($hexColor,1,1).substr($hexColor,1,1));
            $b = hexdec(substr($hexColor,2,1).substr($hexColor,2,1));
            $a = hexdec(substr($hexColor,3,1).substr($hexColor,3,1));
        } else {
            throw new ColorException('invalid hex color :'.$hexColor);
        }
        $result = new Color($r/255,$g/255,$b/255,$a/255);
        return $result;
    }

    public function __construct($r,$g,$b,$a=1) {
        $this->red = $r;
        $this->green = $g;
        $this->blue = $b;
        $this->alpha = $a;
    }
    public function red($value = null) {
        if ($value === null) {
            return $this->red;
        }
        $this->red = $value;
        return $this;
    }

    public function green($value = null) {
        if ($value === null) {
            return $this->green;
        }
        $this->green = $value;
        return $this;
    }

    public function blue($value = null) {
        if ($value === null) {
            return $this->blue;
        }
        $this->blue = $value;
        return $this;
    }

    public function alpha($value = null) {
        if ($value === null) {
            return $this->alpha;
        }
        $this->alpha = $value;
        return $this;
    }

    public function toHexRGB() {
        return '#'.$this->hexComponent($this->red).$this->hexComponent($this->green).$this->hexComponent($this->blue);
    }

    public function toHexRGBA() {
        return '#'.$this->hexComponent($this->red).$this->hexComponent($this->green).$this->hexComponent($this->blue).$this->hexComponent($this->alpha);
    }

    public function getComponent($c) {
        return $this->{$c};
    }

    public function distanceFrom(Color $c) {
        $r1 = $this->red;
        $g1 = $this->green;
        $b1 = $this->blue;
        $r2 = $c->red();
        $g2 = $c->green();
        $b2 = $c->blue();
        return sqrt(($r1-$r2)*($r1-$r2) + ($g1-$g2)*($g1-$g2) + ($b1-$b2)*($b1-$b2));
    }



    protected function hexComponent($input) {
        $result = dechex(round($input*255));
        if (strlen($result) == 1){
            $result = '0'.$result;
        }
        return $result;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return $this->toHexRGB();
    }
}
