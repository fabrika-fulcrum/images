<?php
namespace Fulcrum\Images\Geometry;

use Fulcrum\Images\Exception\GeometryException;

class Point {
    protected $x;
    protected $y;

    public static function Create($x, $y) {
        return new static($x, $y);
    }

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(){
        return $this->x;
    }

    public function getY(){
        return $this->y;
    }
}
