<?php
namespace Fulcrum\Images\Geometry;

use Fulcrum\Images\Exception\GeometryException;

class Box {
    protected $width;
    protected $height;

    public static function Create($width, $height) {
        return new static($width, $height);
    }

    public function __construct($width, $height)
    {
        if ($width <=0 || $height<=0) {
            throw new GeometryException('Invalid dimensions. Width or height cannot be zero or negative');
        }
        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth(){
        return $this->width;
    }

    public function getHeight(){
        return $this->height;
    }

    public function isSquare() {
        return $this->width == $this->height;
    }

    public function isVertical(){
        return $this->height > $this->width;
    }

    public function isHorizontal(){
        return $this->width > $this->height;
    }

    public function getRatio(){
        return $this->width/$this->height;
    }

    public function scale($factor) {
        return new static($this->width * $factor, $this->height * $factor);
    }

    public function scaleToWidth($newWidth) {
        return new static($newWidth, $this->height * $newWidth/$this->width);
    }

    public function scaleToHeight($newHeight) {
        return new static($this->width * $newHeight/$this->height, $newHeight);
    }

    

    public function transpose(){
        return new static($this->height,$this->width);
    }

    public function fitsInside(Box $container) {
        return $this->width <= $container->getWidth() && $this->height <= $container->getHeight();
    }

    public function fitInto(Box $container) {
        $res = $this->scaleToWidth($container->getWidth());
        if ($res->getHeight() > $container->getHeight()) {
            $res = $res->scaleToHeight($container->getHeight());
        }
        return $res;
    }

}
