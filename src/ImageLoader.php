<?php
namespace Fulcrum\Images;

use Fulcrum\Filesystem\Path;
use GuzzleHttp\Client;
use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Exception\InvalidSourceException;
use Fulcrum\Images\Geometry\Box;
use Imagick;
use Fulcrum\Http\RequestFile;

class ImageLoader
{
    /** @var ImageType */
    public $type;
    /** @var string */
    public $filename;
    /**
     * @var string
     * contains svg data
     */
    public $rawSource;
    /**
     * @var Imagick
     * contains raster image
     */
    public $source;
    public $animated = false;
    public $transparent = false;

    public function __construct($src)
    {
        //_d($src,'imageLoader Construct');
        $this->loadSource($src);

        if ($this->type == ImageType::GIF) {
            $this->animated = preg_match('#(\x00\x21\xF9\x04.{4}\x00\x2C.*){2,}#s', $this->rawSource);
            //_d($this->animated,'is animated');
        }
        //_d($this->source->getImageAlphaChannel(), 'transparent channel');
        $this->transparent = $this->source->getImageAlphaChannel();
        $this->adjustOrientation();
    }

    /**
     * @param string|Path|RequestFile $src
     * @return null
     * @throws ImagesException
     * @throws InvalidSourceException
     */
    protected function loadSource($src)
    {
        //_d($src,'loadSource');
        if ($src === null) {
            throw new InvalidSourceException('Image Source not set');
        }

        if (is_string($src)) {
            if (filter_var($src, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                $this->loadFromUrl($src);
                return null;
            }
            if (Path::Create($src)->exists()) {
                $this->loadFromPath(Path::Create($src));
                return null;
            }
            if (substr($src, 0, strlen('data:image')) == 'data:image') {
                $this->loadFromDataUrl($src);
                return null;
            }
        }
        if (is_a($src, RequestFile::class)) {
            $this->loadFromUploadedFile($src);
            return null;
        }
        if (is_a($src, Path::class)) {
            $this->loadFromPath($src);
            return null;
        }
        throw new InvalidSourceException('Unable to recognize image source');
    }


    protected function loadFromUrl($url)
    {
        $types = [
            'image/jpeg' => ImageType::JPG,
            'image/jpg' => ImageType::JPG,
            'image/png' => ImageType::PNG,
            'image/gif' => ImageType::GIF,
            'image/svg+xml' => ImageType::SVG
        ];

        try {
            $response = (new Client())->get($url);

            if ($response->getStatusCode() !== 200) {
                throw new InvalidSourceException('Url could not be reached or did not return valid data');
            }

            $contentType = $response->getHeader('content-type')[0];
            if (!array_key_exists($contentType, $types)) {
                throw new InvalidSourceException('Data type could not be understood');
            }

            $i = new Imagick($url);


            $this->type = $types[$contentType];
            if ($this->type === null) {
                throw new InvalidSourceException('Unrecognized image type');
            }
            if ($this->type == ImageType::SVG || $this->type == ImageType::GIF) {
                $this->rawSource = file_get_contents($url);
            }
            $this->filename = Path::Create($url)->filename();
            $this->source = $i;

        } catch (\ImagickException $e) {

        }


    }

    protected function loadFromPath(Path $path)
    {
        $i = new Imagick($path->__toString());
        $this->type = ImageType::getTypeByExtension($path->extension());
        if ($this->type == ImageType::SVG || $this->type == ImageType::GIF){
            $this->rawSource = $path->read();
        }
        $this->filename = $path->filename();
        $this->source = $i;
    }

    protected function loadFromDataUrl($dataUrl)
    {
        $base64Headers = [
            'jpg' => 'data:image/jpeg;base64',
            'png' => 'data:image/png;base64',
            'gif' => 'data:image/gif;base64',
            'svg' => 'data:image/svg+xml;base64'
        ];

        $sourceParts = explode(',', $dataUrl);
        if (count($sourceParts) != 2) {
            throw new InvalidSourceException('Invalid DataUrl source contents');
        }

        foreach ($base64Headers as $type => $header) {
            if ($sourceParts[0] == $header) {
                $this->filename = 'image';
                $this->type = ImageType::getTypeByExtension($type);
                if ($this->type == ImageType::SVG || $this->type == ImageType::GIF) {
                    $this->rawSource = base64_decode($sourceParts[1]);
                }
                $stream = fopen($dataUrl, 'r');
                $i = new Imagick();
                $i->readImageFile($stream);
                fclose($stream);
                $this->source = $i;
            }
        }

        throw new InvalidSourceException('Unrecognized DataUrl type');
    }

    protected function loadFromUploadedFile(RequestFile $requestFile)
    {
        $i = new Imagick($requestFile->getTmpName());
        $this->type = ImageType::getTypeByMimeType($requestFile->getType());
        if ($this->type == ImageType::SVG || $this->type == ImageType::GIF) {
            $this->rawSource = file_get_contents($requestFile->getTmpName());
        }
        $this->filename = $requestFile->getName();
        $this->source = $i;
    }

    protected function adjustOrientation(){
        if (is_a($this->source, Imagick::class)) {
            $orientation = method_exists($this->source, 'getImageOrientation')?$this->source->getImageOrientation():0;
            switch ($orientation){
                case Imagick::ORIENTATION_BOTTOMRIGHT:
                    $this->source->rotateImage("#000", 180);
                    $this->source->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                break;

                case Imagick::ORIENTATION_RIGHTTOP:
                    $this->source->rotateImage("#000", 90);
                    $this->source->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                break;

                case Imagick::ORIENTATION_LEFTBOTTOM:
                    $this->source->rotateImage("#000", -90);
                    $this->source->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);
                break;
            }

        }
    }
    
    public function conformTo(Box $bounding) {
        if ($this->type == ImageType::SVG || ($this->type == ImageType::GIF && $this->animated)) {
            return null;
        }
        $size = Box::Create($this->source->getImageWidth(),$this->source->getImageHeight());
        if (!$size->fitsInside($bounding)) {
            $newSize = $size->fitInto($bounding);
            $this->source->resizeImage($newSize->getWidth(),$newSize->getHeight(), \Imagick::FILTER_LANCZOS,1);
        }
    }

}
