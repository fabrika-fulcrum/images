<?php
namespace Fulcrum\Images;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\ImagesException;

class ImageManager {

    /**var RepositoryList **/
    protected $repositories;

    /**
     * @var ImagesConfig
     */
    protected $config;

    public function __construct()
    {
        $this->repositories = new RepositoryList();
    }

    public function registerRepository($name) {
        if (is_string($name)) {
            $repository = new Repository($name);
        } else if (is_a($name, Repository::class)) {
            $repository = $name;
        } else {
            throw new ImagesException('Invalid Repository');
        }
        $repository->setManager($this);
        $this->repositories[] = $repository;
        return $repository;
    }

    /**
     * @param ImagesConfig|null $imagesConfig
     * @return $this|ImagesConfig
     */
    public function setConfig(ImagesConfig $imagesConfig)
    {
        $this->config = $imagesConfig;
        return $this;
    }

    public function getConfig(){
        return $this->config;
    }
    
    public function getDefaultRepository() {
        return $this->repositories->getDefault();
    }

    public function getRepository($name) {
        return $this->repositories[$name] ?? null;
    }
    
    public function createImage($src){
        return $this->repositories->getDefault()->createImage($src);
    }
    
    public function find($id){
       return $this->repositories->getDefault()->find($id);
    }


    public function findByUrl($url) {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getConfig()->getPublicPath());

        $repository =  $this->getRepository($path->segment(0));
        if (!$repository) {
            return false;
        }
        return $repository->findByUrl($url);
    }

    public function findFormatByUrl($url) {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getConfig()->getPublicPath());

        $repository =  $this->getRepository($path->segment(0));
        if (!$repository) {
            return false;
        }
        return $repository->findFormatByUrl($url);
    }

}
