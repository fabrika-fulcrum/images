<?php
namespace Fulcrum\Images\Record;

use Fulcrum\Images\Repository;

Interface ImageRecordInterface {
    public function setImage($image);
    public function getImage();
    public function setWidth($width);
    public function getWidth();
    public function setHeight($height);
    public function getHeight();
    public function setLabel($label);
    public function getLabel();
    public function setMetadata($metadata);
    public function getMetadata();
}
