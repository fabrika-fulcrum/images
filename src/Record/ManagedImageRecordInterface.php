<?php
namespace Fulcrum\Images\Record;

use Fulcrum\Images\Image;
use Fulcrum\Images\Repository;

Interface ManagedImageRecordInterface extends ImageRecordInterface{
    public function save();
    public function load($id, Image $image=null);
    public function delete();
}
