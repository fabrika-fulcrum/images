<?php
namespace Fulcrum\Images\Record;

use Fulcrum\Images\Repository;

class DummyRecord implements ImageRecordInterface {

    public function setImage($image)
    {
        // TODO: Implement setImage() method.
    }

    public function getImage()
    {
        // TODO: Implement getImage() method.
    }

    public function setWidth($width)
    {
        // TODO: Implement setWidth() method.
    }

    public function getWidth()
    {
        return null;
    }

    public function setHeight($height)
    {
        // TODO: Implement setHeight() method.
    }

    public function getHeight()
    {
        return null;
    }

    public function setLabel($label)
    {
        // TODO: Implement setLabel() method.
    }

    public function getLabel()
    {
        return null;
    }

    public function setMetadata($metadata)
    {
        // TODO: Implement setMetadata() method.
    }

    public function getMetadata()
    {
        return [];
    }

    public function setAnimated($animated)
    {
        // TODO: Implement setAnimated() method.
    }

    public function getAnimated()
    {
        return null;
    }

    public function setTransparent($transparent)
    {
        // TODO: Implement setTransparent() method.
    }

    public function getTransparent()
    {
        return null;
    }
}
