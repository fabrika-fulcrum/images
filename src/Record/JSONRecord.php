<?php
namespace Fulcrum\Images\Record;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Image;
use Fulcrum\Images\Repository;

class JSONRecord implements ManagedImageRecordInterface {

    protected $width;
    protected $height;
    protected $label;
    protected $animated;
    protected $transparent;
    protected $metadata = [];
    /**
     * @var Image;
     */
    protected $image;

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }
    
    public function save()
    {
        $path = Path::Create($this->image->getRepository()->getSourceDir()->append($this->image->getId())->__toString() . '.json');
        $data = [
            'width' => $this->width,
            'height' => $this->height,
            'label' => $this->label,
            'animated' => $this->animated ? true : false,
            'transparent' => $this->transparent ? true : false,
            'metadata' => $this->metadata
        ];
        $path->write(json_encode($data));
    }

    public function load($id, Image $image=null) {
        $path = Path::Create($image->getRepository()->getSourceDir()->append($id)->__toString().'.json');
        if (!$path->exists()){
            return;
        }
        $data = json_decode($path->read());
        $this->width = $data->width;
        $this->height = $data->height;
        $this->label = $data->label;
        $this->animated = $data->animated;
        $this->transparent = $data->transparent;
        $this->metadata = $data->metadata;
    }

    public function delete()
    {
        $path = Path::Create($this->image->getRepository()->getSourceDir()->append($this->image->getId())->__toString() . '.json');
        @unlink($path);
    }


    public function setAnimated($animated)
    {
        $this->animated = $animated;
        return $this;
    }

    public function getAnimated()
    {
        return $this->animated;
    }

    public function setTransparent($transparent)
    {
        $this->transparent = $transparent;
        return $this;
    }

    public function getTransparent()
    {
        return $this->transparent;
    }

}
