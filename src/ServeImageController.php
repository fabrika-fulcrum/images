<?php
namespace Fulcrum\Images;

class ServeImageController
{
    
    public function missingImageAction($repo, $format, $file, $ext)
    {
        $img = Images::getRepository($repo)->find($file.'.'.$ext);
        if ($img) {
            $img->applyFormat($format);
            $output = $img->getFormattedOutput();
            http_response_code(200);
            header("Content-Type: " . $output->type);
            header("Content-Length: " . strlen($output->contents));
            echo $output->contents;
        } else {
            http_response_code(404);
        }
        die();
    }
}
