<?php
namespace Fulcrum\Images;

use Fulcrum\Filesystem\Path;
use Images\Geometry\Box;

class ImagesConfig
{
    /** @var Path */
    protected $sourcePath = '';

    /** @var Path */
    protected $outputPath = '';

    /** @var Path */
    protected $protectedOutputPath = '';

    /** @var Path */
    protected $publicPath = '';

    /** @var Path */
    protected $protectedPublicPath = '';

    protected $conformWidth = 1500;

    protected $conformHeight = 1500;

    protected $defaultQuality = 60;

    protected $passThrough = false;

    /**
     * @return Path
     */
    public function getSourcePath()
    {
        return $this->sourcePath;
    }

    /**
     * @return Path
     */
    public function getOutputPath()
    {
        return $this->outputPath;
    }

    /**
     * @return Path
     */
    public function getProtectedOutputPath()
    {
        return $this->protectedOutputPath;
    }

    /**
     * @return Path
     */
    public function getPublicPath()
    {
        return $this->publicPath;
    }

    /**
     * @return Path
     */
    public function getProtectedPublicPath()
    {
        return $this->protectedPublicPath;
    }

    /**
     * @return int
     */
    public function getConformWidth()
    {
        return $this->conformWidth;
    }

    /**
     * @return int
     */
    public function getConformHeight()
    {
        return $this->conformHeight;
    }

    /**
     * @return int
     */
    public function getDefaultQuality()
    {
        return $this->defaultQuality;
    }

    /**
     * @return boolean
     */
    public function getPassThrough()
    {
        return $this->passThrough;
    }


    /**
     * @param Path $sourcePath
     * @return $this
     */
    public function setSourcePath($sourcePath)
    {
        $this->sourcePath = $sourcePath;
        return $this;
    }

    /**
     * @param Path $outputPath
     * @return $this
     */
    public function setOutputPath($outputPath)
    {
        $this->outputPath = $outputPath;
        return $this;
    }

    /**
     * @param Path $protectedOutputPath
     * @return $this
     */
    public function setProtectedOutputPath($protectedOutputPath)
    {
        $this->protectedOutputPath = $protectedOutputPath;
        return $this;
    }

    /**
     * @param Path $publicPath
     * @return $this
     */
    public function setPublicPath($publicPath)
    {
        $this->publicPath = $publicPath;
        return $this;
    }

    /**
     * @param Path $protectedOutputPath
     * @return $this
     */
    public function setProtectedPublicPath($protectedOutputPath)
    {
        $this->protectedPublicPath = $protectedOutputPath;
        return $this;
    }

    public function setConformWidth($conformWidth)
    {
        $this->conformWidth = $conformWidth;
        return $this;
    }

    public function setConformHeight($conformHeight)
    {
        $this->conformHeight = $conformHeight;
        return $this;
    }

    public function setConformSize($conformWidth, $conformHeight)
    {
        $this->conformWidth = $conformWidth;
        $this->conformHeight = $conformHeight;
        return $this;
    }

    public function getConformSize()
    {
        return Box::Create($this->conformWidth, $this->conformHeight);
    }

    public function setDefaultQuality($defaultQuality)
    {
        $this->defaultQuality = $defaultQuality;
        return $this;
    }

    public function setPassThrough($passThrough)
    {
        $this->passThrough = $passThrough;
        return $this;
    }

}
