<?php
namespace Fulcrum\Images;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\CannotChangeStorageMethodException;
use Fulcrum\Images\Exception\ImagesException;
use Fulcrum\Images\Exception\InvalidRecordClassException;
use Fulcrum\Images\Format\AbstractFormat;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Preop\PreopInterface;
use Fulcrum\Images\Record\DummyRecord;
use Fulcrum\Images\Record\ImageRecordInterface;
use Fulcrum\Images\StorageMethod\AbstractStorageMethod;
use Fulcrum\Images\StorageMethod\ManyToOne;
use Fulcrum\Images\StorageMethod\OneToOne;
use Fulcrum\Images\StorageMethod\Shared;

class Repository
{
    /**
     * @var ImageManager
     */
    protected $manager;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var AbstractStorageMethod
     */
    protected $storageMethod;
    /**
     * @var ImageRecordInterface
     */
    protected $recordClass;
    /**
     * @var FormatList
     */
    protected $formats = [];

    protected $protected = false;
    protected $default = false;
    
    /** @var PreopInterface[] */
    protected $preops = [];

    public function __construct($name = '')
    {
        $this->name($name);
        $this->recordClass = DummyRecord::class;
        $this->formats = new FormatList();
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    public function name($name = null)
    {
        if ($name === null) {
            return $this->name;
        }
        $this->name = $name;
        return $this;
    }

    public function setDefault($default = true){
        $this->default = $default;
    }

    public function isDefault(){
        return $this->default;
    }

    protected function setStorageMethod(AbstractStorageMethod $method)
    {
        if ($this->storageMethod !== null) {
            throw new CannotChangeStorageMethodException('Cannot Change Storage Method for repository ' . $this->name);
        }
        $this->storageMethod = $method;
        $this->storageMethod->setRepository($this);
    }

    public function shared()
    {
        $this->setStorageMethod(new Shared());
        return $this;
    }

    public function manyToOne()
    {
        $this->setStorageMethod(new ManyToOne());
        return $this;
    }

    public function oneToOne()
    {
        $this->setStorageMethod(new OneToOne());
        return $this;
    }

    public function storeAs(AbstractStorageMethod $method) {
        $this->setStorageMethod($method);
        return $this;
    }

    public function addPreop(PreopInterface $preop) {
        $this->preops[] = $preop;
        return $this;
    }

    public function addFormat(AbstractFormat $format)
    {
        $this->formats[] = $format;
        $format->repository($this);
        return $this;
    }

    public function protect($protection)
    {
        $this->protected = true;
        //TODO: Add $protection middleware
    }

    /**
     * @param $recordClass
     * @return $this
     * @throws InvalidRecordClassException
     */
    public function setRecordClass($recordClass) {
        if (!class_exists($recordClass)) {
            throw new InvalidRecordClassException('Could not find class '.$recordClass);
        }
        $this->recordClass = $recordClass;
        return $this;
    }

    public function hasFormat($format)
    {
        return isset($this->formats[$format]);
    }

    public function getFormat($format)
    {
        return $this->formats[$format];
    }

    public function getFormats(){
        return $this->formats;
    }

    public function getSourceDir()
    {
        return Path::CreateDir($this->getManager()->getConfig()->getSourcePath())->appendDir($this->name());
    }

    public function getOutputDir()
    {
        return Path::CreateDir($this->getManager()->getConfig()->getOutputPath())->appendDir($this->name());
    }

    public function createImage($src) {
        $img = new Image();
        $img->setRepository($this);
        $loader = new ImageLoader($src);
        $loader->conformTo(Box::Create($this->manager->getConfig()->getConformWidth(),$this->manager->getConfig()->getConformWidth()));

        $img->setLoader($loader);
        $img->setRecord(new $this->recordClass);
        return $img;
    }
    
    public function storeImage(Image $img) {
        $storeName = $this->storageMethod->getNewNameForImage($img);
        $storePath = $this->getSourceDir()->append($storeName);
        Path::CreateDir($storePath->directory())->mkdir();
        if ($img->getType() == ImageType::SVG || ($img->getType() == ImageType::GIF && $img->isAnimated())) {
            file_put_contents($storePath,$img->getLoader()->rawSource);
        } else {
        	if ($storePath->exists()) {
        		$storePath->delete();
			}
            if ($img->getLoader()->source->writeImage($storePath) !== true) {
        		throw new ImagesException(sprintf('Cannot write image to %s',$storePath));
			}
        }
        foreach ($this->preops as $preop) {
            $preop->process($img);
        }
        $img->setId($storeName);
    }

    public function find($id)
    {
        $storedPath = $this->storageMethod->findById($id);
        if (!$storedPath) {
            return null;
        }
        return $this->getImageFromStoredPath($storedPath);
    }

    public function findByUrl($url)
    {
        $storedPath = $this->storageMethod->findByUrl($url);
        if (!$storedPath) {
            return null;
        }
        return $this->getImageFromStoredPath($storedPath);
    }

    /**
     * @param $url
     * @return AbstractFormat|null
     * @throws \Filesystem\Exception\InvalidPathOperationException
     */
    public function findFormatByUrl($url) {
        $path = Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])->stemFrom($this->getManager()->getConfig()->getPublicPath());

        return $this->formats[$path->segment(1)];
    }

    protected function getImageFromStoredPath(Path $storedPath) {
        $img = new Image();
        $img->setRepository($this);
        $img->setId($storedPath->basename());
        $img->setRecord(new $this->recordClass);
        $img->loadRecord();
        $img->loadSource($storedPath);
        return $img;
    }

    public function delete(Image $image) {
        $path = $image->getSourcePath();
        @unlink($path);
        foreach ($this->getFormats() as $format){
            $path = $format->getPathFor($image);
            @unlink($path);
        }
    }

}
