<?php
namespace Fulcrum\Images;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Exception\MissingFormatException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Record\ImageRecordInterface;
use Fulcrum\Images\Record\ManagedImageRecordInterface;
use Imagick;

class Image
{
    /**
     * @var ImageRecordInterface
     */
    protected $record;

    protected $loader;
    protected $source;
    protected $sourcePath;

    /**
     * @var ImageOutput
     */
    protected $output;
    
    /**
     * @var string
     */
    protected $id;

    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @var Box
     */
    protected $size;

    /**
     * @var ImageType
     */
    protected $type;

    protected $animated = false;
    protected $transparent = false;

    protected $parentId;

    protected $label;

    protected $metadata = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Image
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param Repository $repository
     * @return Image
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /**
     * @return Box
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Box $size
     * @return Image
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return ImageType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param ImageType $type
     * @return Image
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function hasParent()
    {
        return $this->parentId !== null;
    }

    /**
     * @return ImageLoader
     */
    public function getLoader()
    {
        return $this->loader;
    }

    public function isAnimated()
    {
        return $this->animated;
    }

    public function isTransparent()
    {
        return $this->transparent;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setMetadata($key, $value)
    {
        $this->metadata[$key] = $value;
        return $this;
    }

    public function getMetadata($key = null)
    {
        if ($key === null) {
            return $this->metadata;
        }
        if (array_key_exists($key, $this->metadata)) {
            return $this->metadata[$key];
        }
        return null;
    }

    public function hasMetadata($key)
    {
        return array_key_exists($key, $this->metadata);
    }

    /**
     * @return Imagick
     */
    public function getSource(){
        return $this->source;
    }

    /**
     * @return ImageRecordInterface
     */
    public function getRecord() {
        return $this->record;
    }
    /**
     * @param $loader
     * @return $this
     */
    public function setLoader(ImageLoader $loader)
    {
        $this->loader = $loader;
        $this->type = $loader->type;
        $this->animated = $loader->animated;
        $this->transparent = $loader->transparent;
        $this->size = new Box($this->loader->source->getImageWidth(), $this->loader->source->getImageHeight());
        //_d($this->size, 'size from loader');
        $this->label = $loader->filename;
        return $this;
    }

    public function setRecord(ImageRecordInterface $record)
    {
        $this->record = $record;
        if(!$record->getImage()) {
            $this->record->setImage($this);
        }
        return $this;
    }

    protected function updateRecord()
    {
        $this->metadata['animated'] = $this->animated;
        $this->metadata['transparent'] = $this->transparent;
        $this->record->setWidth($this->size->getWidth());
        $this->record->setHeight($this->size->getHeight());
        $this->record->setLabel($this->label);
        $this->record->setMetadata($this->metadata);
    }

    protected function updateFromRecord()
    {
        //_d($this, 'update from record');
        try {
            $this->size = Box::Create($this->record->getWidth(), $this->record->getHeight());
        } catch (GeometryException $e) {
            $this->size = null;
        }
        $this->label = $this->record->getLabel();
        $this->metadata = $this->record->getMetadata();
        $this->animated = $this->getMetadata('animated');
        $this->transparent = $this->getMetadata('transparent');

    }

    public function save()
    {
        if ($this->id === null) {
            $this->repository->storeImage($this);
        }
        $this->updateRecord();
        if (is_a($this->record, ManagedImageRecordInterface::class)) {
            $this->record->save();
        }
    }

    public function loadRecord()
    {
        if (is_a($this->record, ManagedImageRecordInterface::class)) {
            $this->record->load($this->id, $this);
        }
        $this->updateFromRecord();
    }

    /**
     * @return Path
     */
    public function getSourcePath(){
        return $this->sourcePath;
    }
    
    public function loadSource($sourcePath)
    {
        $this->sourcePath = $sourcePath;
        $ext = Path::Create($sourcePath)->extension();
        $this->type = ImageType::getTypeByExtension(''.$ext);
        $this->source = new Imagick($sourcePath->__toString());
        if ($this->size === null) {
            $this->size = Box::Create($this->source->getImageWidth(), $this->source->getImageHeight());
        }
    }

    public function applyFormat($formatName)
    {
        if (!$this->repository->hasFormat($formatName)) {
            throw new MissingFormatException('Missing format "' . $formatName . '" in repository"');
        }

        $this->output = $this->repository->getFormat($formatName)->process($this);
    }
    
    public function getFormattedOutput() {
        return $this->output;
    }
    
    public function getFormattedPath($formatName) {
        if (!$this->repository->hasFormat($formatName)) {
            throw new MissingFormatException('Missing format "' . $formatName . '" in repository"');
        }
        return $this->repository->getFormat($formatName)->getPathFor($this)
            ->stemFrom($this->repository->getManager()->getConfig()->getOutputPath())
            ->prepend($this->repository->getManager()->getConfig()->getPublicPath())
            ->stemFrom($_SERVER['DOCUMENT_ROOT']);
    }

    public function delete(){
        $this->getRepository()->delete($this);
        if (is_a($this->record, ManagedImageRecordInterface::class)) {
            $this->record->deleteRecord();
        }
    }
}
