<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Analysis\DetectFaces;
use Fulcrum\Images\Analysis\DetectFeatures;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Geometry\Point;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\ImageType;
use Fulcrum\Http\Mimes;

/**
 * Class Crop
 * Crops image from center
 * @package Images\Format
 */
class SmartCrop extends AbstractFormat {

    public function processImage(Image $image, $params=[]) {
        $width = $params['width'] ?? $this->width;
        $height = $params['height'] ?? $this->height;
        $quality = $params['quality'] ?? $this->quality;

        $originalBox = $image->getSize();

        $destinationBox = Box::Create($width, $height);
        $destinationBox = $destinationBox->fitInto($originalBox);

        if ($image->hasMetadata('features') && array_key_exists('details', $image->getMetadata('features'))) {
            $features = $image->getMetadata('features');
        } else {
            $features = (new DetectFeatures())->calculateFeaturesMap($image->getSource(),50);
            $image->setMetadata('features', $features);
            $image->save();
        }

        // Face detection
        if ($image->hasMetadata('faces')) {
            $faces = $image->getMetadata('faces');
        } else {
            $faces = (new DetectFaces())->performDetection($image);
            $image->setMetadata('faces', $faces);
            $image->save();
        }

        $fW = count($features['details']);
        $fH = count($features['details'][0]);
        $featuresBox = Box::create($fW, $fH);

        $facesBox = [];
        for ($x = 0; $x < $fW; $x++) {
            $facesBox[$x] = array_fill(0, $fH, 0);
        }

        foreach ($faces as $face) {
            for ($x = 0; $x<floor($fW/$face['w']); $x++) {
                for ($y = 0; $y<floor($fH/$face['h']); $y++) {
                    $facesBox[$x][$y] = 1;
                }
            }
        }


        $minScale = min(max(1, $width/$destinationBox->getWidth()),1);
        $maxScale = 1;
        $scaleStep = ($minScale < $maxScale) ? 1/((1-$minScale)*min($fW,$fH)) * 2 : 1;


        $best = new class {
            public $score = -99999999999999;
            public $scale = 1;
            public $x;
            public $y;

            public function mark($score, $scale, $x, $y) {
                if ($score > $this->score){
                    $this->score = $score;
                    $this->scale = $scale;
                    $this->x = $x;
                    $this->y = $y;
                }
            }
        };

        $scale = $minScale;
        do {
            $scoreBox = $destinationBox->fitInto($featuresBox)->scale($scale);
            $scoreMask = $this->generateScoringWindow($scoreBox);
            $localBest = -9999999999999;
            for ($slideX =0; $slideX<=$fW-$scoreBox->getWidth(); $slideX++) {
                for ($slideY = 0; $slideY<= $fH-$scoreBox->getHeight(); $slideY++) {
                    $score = 0;
                    for ($x = 0; $x < $scoreBox->getWidth(); $x++) {
                        for ($y = 0; $y < $scoreBox->getHeight(); $y++) {
                            $faceVal = $facesBox[$x+$slideX][$y+$slideY];
                            $featureVal = $features['details'][$x+$slideX][$y+$slideY];
                            $luminanceVal = $features['luminance'][$x+$slideX][$y+$slideY];
                            $saturation = $features['saturation'][$x+$slideX][$y+$slideY];
                            $maskVal = $scoreMask[$x][$y];
                            $score += $faceVal * 2 * $maskVal;
                            $score += $featureVal * $maskVal;
                            $score += $saturation * 0.5 * $maskVal;
                            $score += $luminanceVal * 0.5 * $maskVal;
                        }
                    }

                    if ($score > $localBest) {
                        $localBest = $score;
                    }
                    $score = $score / ($scoreBox->getWidth() * $scoreBox->getHeight());

                    $best->mark($score, $scale, $slideX, $slideY);
                }
            }

            $scale+=$scaleStep;
        } while ($scale <= $maxScale);

        $origin = new Point(
            round($originalBox->getWidth()/$featuresBox->getWidth()*$best->scale*$best->x),
            round($originalBox->getHeight()/$featuresBox->getHeight()*$best->scale*$best->y)
        );
        $cropBox = $destinationBox->scale($best->scale);

        $destinationResizeBox = Box::Create($width, $height);
        $output = new ImageOutput();
        $output->type = Mimes::getType($image->getType()->value());

        if ($image->getType() == ImageType::SVG) {

            $output->contents = $image->getSourcePath()->read();
        } else {
            $destImg = clone($image->getSource());
            $destImg->cropImage($cropBox->getWidth(), $cropBox->getHeight(), $origin->getX(), $origin->getY());
            $destImg->resizeImage($destinationResizeBox->getWidth(), $destinationResizeBox->getHeight(), \Imagick::FILTER_LANCZOS, 1);

            if ($image->getType() == ImageType::JPG) {
                $destImg->setImageCompression(\Imagick::COMPRESSION_JPEG);
                $destImg->setCompressionQuality((int)$quality);
                $destImg->setImageFormat('jpg');
            } else if ($image->getType() == ImageType::PNG) {
                $destImg->setImageCompressionQuality(floor(($quality*0.9)/10));
                $destImg->setImageFormat('png');
            }
            $destImg->stripImage();

            $output->contents = $destImg->getImageBlob();
        }
        return $output;
    }

    public function store(Image $source, ImageOutput $output) {
        $storePath = $this->repository->getOutputDir()->appendDir($this->name())->append($source->getId()); //FIXME Needs to get output path from somewhere in the StorageMethod
        if (!$storePath->directory()->exists()) {
            $storePath->directory()->mkdir();
        }

        if ($source->getType() == ImageType::SVG) {
            $storePath->write($source->getSourcePath()->read());
        } else {
            $storePath->write($output->contents);
        }
        return $storePath;
    }

    protected function generateScoringWindow(Box $box) {
        $width = $box->getWidth();
        $height = $box->getHeight();
        $pixels = [];

        for ($iX = 0; $iX < $width; $iX++) {
            $pixels[$iX] = [];
            for ($iY = 0; $iY < $height; $iY++) {
                $pxValue = $this->scoreFormula(($iX/$height)-0.5, ($iY/$height)-0.5);
                $pixels[$iX][$iY] = $pxValue;
            }
        }

        return $pixels;
    }

    private function scoreFormula($x, $y)
    {
        return $this->scoreModuleFormula($x)
            + $this->scoreModuleFormula($y)
            + 2 *$this->scoreDampenFormula($x,$y)
            - 3 * ($this->scoreModuleFormula($x,0.5)+$this->scoreModuleFormula($y,0.5));
    }

    private function scoreModuleFormula($x, $shift=0.1666666, $base=2, $power=2, $squeeze = 20, $coefficient=0.5) {
        return $coefficient*(pow($base, -pow($squeeze*($x + $shift), $power)) + pow($base, -pow($squeeze*($x - $shift), $power)));
    }

    function scoreDampenFormula($x, $y, $base = 2, $power = 2, $squeeze = 1.5, $coefficient=3) {
        return $coefficient * pow($base, -pow($squeeze*$base*$x, $power)) * pow($base, -pow($squeeze*$base*$y, $power));
    }
}
