<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Analysis\DetectBorder;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Geometry\Point;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\ImageType;
use Fulcrum\Http\Mimes;



class Fill extends AbstractFormat {

    /*public function process(Image $image, $params=[])
    {

    }*/

    public function processImage(image $image, $params=[]) {
        $width = $params['width'] ?? $this->width;
        $height = $params['height'] ?? $this->height;
        $quality = $params['quality'] ?? $this->quality;

        $originalBox = $image->getSize();

        if ($image->hasMetadata('border_color')) {
            $borderColor = $image->getMetadata('border_color');
        } else {
            $borderColor = (new DetectBorder())->analyzeBorder($image->getSource(), 120);
        }

        $destinationBox = Box::Create($width, $height);
        $scaleBox = $originalBox->fitInto($destinationBox);
        $destinationBox = $destinationBox->fitInto($originalBox);
        if ($destinationBox->getWidth() > $scaleBox->getWidth()) {
            $origin = new Point(round(($destinationBox->getWidth()-$scaleBox->getWidth())/2), 0);
        } else {
            $origin = new Point(0, round(($destinationBox->getHeight()-$scaleBox->getHeight())/2));
        }
        $output = new ImageOutput();
        $output->type = Mimes::getType($image->getType()->value());

        if ($image->getType() == ImageType::SVG) {
            $output->contents = $image->getSourcePath()->read();
        } else {
            $destImg = clone($image->getSource());
            $destImg->resizeImage($scaleBox->getWidth(), $scaleBox->getHeight(), \Imagick::FILTER_LANCZOS, 1);
            $destImg->borderImage(new \ImagickPixel($borderColor), round(($width-$scaleBox->getWidth()) /2), round(($height-$scaleBox->getHeight()) /2));

            if ($image->getType() == ImageType::JPG) {
                $destImg->setImageCompression(\Imagick::COMPRESSION_JPEG);
                $destImg->setCompressionQuality($quality);
                $destImg->setImageFormat('jpg');
            } else if ($image->getType() == ImageType::PNG) {
                $destImg->setImageCompressionQuality(floor(($quality*0.9)/10));
                $destImg->setImageFormat('png');
            }
            $destImg->stripImage();

            $output->contents = $destImg->getImageBlob();
        }
        return $output;
    }

    public function store(Image $source, ImageOutput $output) {
        $storePath = $this->repository->getOutputDir()->appendDir($this->name())->append($source->getId()); //FIXME Needs to get output path from somewhere in the StorageMethod
        if (!$storePath->directory()->exists()) {
            $storePath->directory()->mkdir();
        }

        if ($source->getType() == ImageType::SVG) {
            $storePath->write($source->getSourcePath()->read());
        } else {
            $storePath->write($output->contents);
        }
        return $storePath;
    }
}
