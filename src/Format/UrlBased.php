<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Geometry\Point;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\ImageType;
use Fulcrum\Http\Mimes;
use Fulcrum\Images\Utils\RegexMatcher;

class UrlBased extends AbstractFormat {

    protected $urlFormat;
    protected $formatMap = [];
    protected $maximumOutput = 0;
    protected $maxWidth;
    protected $maxHeight;
    protected $tolerance;
    protected $constraints = [];

    public function format(string $format) {
        $this->urlFormat = $format;
        return $this;
    }

    public function formatMap(array $map) {
        $this->formatMap = $map;
        return $this;
    }

    public function maximumOutput(int $maxOutput) {
        $this->maximumOutput = $maxOutput;
        return $this;
    }

    public function maxSize(int $width, int $height) {
        $this->maxWidth = $width;
        $this->maxHeight = $height;
        return $this;
    }

    public function outputTolerance($tolerance) {
        $this->tolerance = $tolerance;
        return $this;
    }

    public function constraints($constraints) {
        $this->constraints = $constraints;
        return $this;
    }

    /**
     * @param Image $image
     * @return ImageOutput;
     */
    public function process(Image $image, $params=[])
    {
        $format = new $this->formatMap[$params['format']]($this->name);
        $output = $format->processImage($image, $params);
        $storePath = $this->store($image, $output, $params);
        $output->path = $storePath;
        return $output;
    }

    protected $matcher;

    public function applyFromUrl($url) {
        $this->matcher = new RegexMatcher(
            $this->urlFormat,
            $this->constraints,
            Path::Create($url)->prepend($_SERVER['DOCUMENT_ROOT'])
                ->stemFrom($this->repository->getOutputDir()->appendDir($this->name)).''
        );
        $img = $this->repository->findByUrl(Path::Create($url)->filename($this->matcher->get('id'))->extension($this->matcher->get('type')));

        $params = [
            'id' => $this->matcher->get('id'),
            'width'=>(int)$this->matcher->get('width'),
            'height'=>(int)$this->matcher->get('height'),
            'format'=>$this->matcher->get('format'),
            'quality'=>(int)$this->matcher->get('quality'),
            'type'=>$this->matcher->get('type')
        ];

        return $this->process($img, $params);
    }

    public function store(Image $source, ImageOutput $output, $params) {
        $filename = $this->matcher->apply($params);

        $storePath = $this->repository->getOutputDir()->appendDir($this->name())->append($filename);
        if (!$storePath->directory()->exists()) {
            $storePath->directory()->mkdir();
        }

        if ($source->getType() == ImageType::SVG) {
            $storePath->write($source->getSourcePath()->read());
        } else {
            $storePath->write($output->contents);
        }
        return $storePath;
    }
}
