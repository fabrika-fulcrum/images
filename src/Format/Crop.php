<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Geometry\Point;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\ImageType;
use Fulcrum\Http\Mimes;

/**
 * Class Crop
 * Crops image from center
 * @package Images\Format
 */
class Crop extends AbstractFormat {

    public function processImage(image $image, $params=[]) {
        $width = $params['width'] ?? $this->width;
        $height = $params['height'] ?? $this->height;
        $quality = $params['quality'] ?? $this->quality;

        $originalBox = $image->getSize();

        $destinationBox = Box::Create($width, $height);
        $destinationBox = $destinationBox->fitInto($originalBox);
        if ($originalBox->getWidth() > $destinationBox->getWidth()) {
            $origin = new Point(round(($originalBox->getWidth()-$destinationBox->getWidth())/2), 0);
        } else {
            $origin = new Point(0, round(($originalBox->getHeight()-$destinationBox->getHeight())/2));
        }
        $destinationResizeBox = Box::Create($width, $height);
        $output = new ImageOutput();
        $output->type = Mimes::getType($image->getType()->value());

        if ($image->getType() == ImageType::SVG) {

            $output->contents = $image->getSourcePath()->read();
        } else {
            $destImg = clone($image->getSource());
            $destImg->cropImage($destinationBox->getWidth(), $destinationBox->getHeight(), $origin->getX(), $origin->getY());
            $destImg->resizeImage($destinationResizeBox->getWidth(), $destinationResizeBox->getHeight(), \Imagick::FILTER_LANCZOS, 1);

            if ($image->getType() == ImageType::JPG) {
                $destImg->setImageCompression(\Imagick::COMPRESSION_JPEG);
                $destImg->setCompressionQuality((int)$quality);
                $destImg->setImageFormat('jpg');
            } else if ($image->getType() == ImageType::PNG) {
                $destImg->setImageCompressionQuality(floor(($quality*0.9)/10));
                $destImg->setImageFormat('png');
            }
            $destImg->stripImage();

            $output->contents = $destImg->getImageBlob();
        }
        return $output;
    }

    public function store(Image $source, ImageOutput $output) {
        $storePath = $this->repository->getOutputDir()->appendDir($this->name())->append($source->getId()); //FIXME Needs to get output path from somewhere in the StorageMethod
        if (!$storePath->directory()->exists()) {
            $storePath->directory()->mkdir();
        }

        if ($source->getType() == ImageType::SVG) {
            $storePath->write($source->getSourcePath()->read());
        } else {
            $storePath->write($output->contents);
        }
        return $storePath;
    }
}
