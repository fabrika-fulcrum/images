<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Filesystem\Path;
use Fulcrum\Images\Exception\GeometryException;
use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\ImageType;
use Fulcrum\Http\Mimes;

/**
 * Class Fit
 * Applies Best Fit Resizing to an image
 * It will resize the input image proportionally,
 * so that the output image fits in the longest side of the output resize box;
 * @package Images\Format
 */
class Fit extends AbstractFormat {


    public function processImage(image $image, $params=[]) {
        $width = $params['width'] ?? $this->width;
        $height = $params['height'] ?? $this->height;
        $quality = $params['quality'] ?? $this->quality;

        $originalBox = $image->getSize();
        $boundingBox = Box::Create($width, $height);

        $destinationBox = $originalBox->fitInto($boundingBox);

        $output = new ImageOutput();
        $output->type = Mimes::getType($image->getType()->value());

        if ($image->getType() == ImageType::SVG) {
            $output->contents = $image->getSourcePath()->read();
        } else {
            $destImg = clone($image->getSource());
            $destImg->resizeImage($destinationBox->getWidth(), $destinationBox->getHeight(), \Imagick::FILTER_LANCZOS, 1);

            if ($image->getType() == ImageType::JPG) {
                $destImg->setImageCompression(\Imagick::COMPRESSION_JPEG);
                $destImg->setImageCompressionQuality($quality);
                $destImg->setImageFormat('jpg');
            } else if ($image->getType() == ImageType::PNG) {
                $destImg->setImageCompressionQuality(floor(($quality/0.99)/10));
                $destImg->setImageFormat('png');
            }
            $destImg->stripImage();

            $output->contents = $destImg->getImageBlob();
        }
        return $output;
    }

    public function store(Image $source, ImageOutput $output) {
        $storePath = $this->repository->getOutputDir()->appendDir($this->name())->append($source->getId()); //FIXME Needs to get output path from somewhere in the StorageMethod
        if (!$storePath->directory()->exists()) {
            $storePath->directory()->mkdir();
        }

        if ($source->getType() == ImageType::SVG) {
            $storePath->write($source->getSourcePath()->read());
        } else {
            $storePath->write($output->contents);
        }
        return $storePath;
    }
}
