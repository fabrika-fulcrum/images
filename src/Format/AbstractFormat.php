<?php
namespace Fulcrum\Images\Format;

use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Image;
use Fulcrum\Images\ImageOutput;
use Fulcrum\Images\Repository;

abstract class AbstractFormat{

    /**
     * @var Repository
     */
    protected $repository;
    protected $name;

    protected $width = 0;
    protected $height = 0;
    protected $quality;

    public function __construct($name = null)
    {
        $this->name = $name;
    }

    public function name($name=null){
        if ($name === null){
            return $this->name;
        }
        $this->name = $name;
        return $this;
    }

    public function width($width=null){
        if ($width === null){
            return $this->width;
        }
        $this->width = $width;
        return $this;
    }

    public function height($height=null){
        if ($height === null){
            return $this->height;
        }
        $this->height = $height;
        return $this;
    }

    /**
     * @param null $width
     * @param null $height
     * @return $this|Box
     */
    public function size($width = null, $height = null) {
        if ($width === null) {
            return Box::Create($this->width, $this->height);
        } 
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    /**
     * @param null $quality
     * @return $this|int
     */
    public function quality($quality=null){
        if ($quality === null){
            if ($this->quality !== null) {
                return $this->quality;
            }
            return $this->repository->getManager()->getConfig()->getDefaultQuality();
        }
        $this->quality = $quality;
        return $this;
    }

    /**
     * @param null $repository
     * @return $this|Repository
     */
    public function repository($repository = null) {
        if ($repository === null){
            return $this->repository;
        }
        $this->repository = $repository;
        return $this;
    }

    protected function canWrite()
    {
        return !$this->repository->getManager()->getConfig()->getPassThrough();
    }

    /**
     * @param Image $image
     * @return ImageOutput;
     */
    public function process(Image $image, $params = []) {
        $output = $this->processImage($image, $params);
        $storePath = $this->store($image, $output);
        $output->path = $storePath;
        return $output;
    }

    public function getPathFor(Image $image) {
        return $this->repository->getOutputDir()->appendDir($this->name())->append($image->getId());
    }

    public function applyFromUrl($url) {
        $img = $this->repository->findByUrl($url);

        return $this->process($img);
    }
    
}
