<?php
namespace Fulcrum\Images\Utils;

class RegexMatcher {

    protected $pattern;
    protected $constraints;
    protected $format;
    protected $subject;
    protected $matches = [];

    public function __construct($format, $constraints,  $subject=null)
    {
        $this->format = $format;
        $this->constraints = $constraints;
        $this->preparePattern();
        $this->subject = $subject;
        if ($this->subject!==null) {
            $this->findMatches();
        }
    }

    protected function preparePattern(){
        $keyPattern = "/({[a-zA-z0-9]+})/";
        $neutralTokens = preg_split($keyPattern, $this->format, -1, PREG_SPLIT_DELIM_CAPTURE);
        $patternParts = [];
        foreach ($neutralTokens as $token) {
            if (preg_match($keyPattern, $token)) {
                $key = substr($token, 1, -1);
                if (array_key_exists($key, $this->constraints)) {
                    $patternParts[] = '(?<'.$key.'>'.$this->constraints[$key].')';
                } else {
                    $patternParts[] = preg_quote($token,'/');
                }
            } else {
                $patternParts[] = preg_quote($token,'/');
            }
        }
        $this->pattern = '/^'.implode('', $patternParts).'$/';
    }

    public function setSubject($subject) {
        $this->subject = $subject;
        $this->findMatches();
        return $this;
    }

    protected function findMatches(){
        $matchesCount = preg_match_all($this->pattern, $this->subject, $matches);
        $this->matches = $matches;
    }

    public function test($string=null) {
        return preg_match($this->pattern, $string ?? $this->subject);
    }

    public function with($testString) {
        return new self($this->format, $this->constraints, $testString);
    }

    public function get($key) {
        return $this->matches[$key][0] ?? null;
    }

    public function apply($params) {
        $result = $this->format;
        foreach ($params as $key=>$value) {
            $result = str_replace('{'.$key.'}',$value,$result);
        }
        return $result;
    }

}
