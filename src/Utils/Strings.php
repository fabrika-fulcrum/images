<?php
namespace Fulcrum\Images\Utils;

class Strings {

    public static function random($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $chars = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $chars[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $chars);
    }

    public static function slugify($string) {
        $transliterateChars = [
            //Latin
            'À'=> 'A', 'Á'=> 'A', 'Â'=> 'A', 'Ã'=> 'A', 'Ä'=> 'A', 'Å'=> 'A', 'Æ'=> 'AE',
            'Ç'=> 'C', 'È'=> 'E', 'É'=> 'E', 'Ê'=> 'E', 'Ë'=> 'E', 'Ì'=> 'I', 'Í'=> 'I',
            'Î'=> 'I', 'Ï'=> 'I', 'Ð'=> 'D', 'Ñ'=> 'N', 'Ò'=> 'O', 'Ó'=> 'O', 'Ô'=> 'O',
            'Õ'=> 'O', 'Ö'=> 'O', 'Ő'=> 'O', 'Ø'=> 'O', 'Ù'=> 'U', 'Ú'=> 'U', 'Û'=> 'U',
            'Ü'=> 'U', 'Ű'=> 'U', 'Ý'=> 'Y', 'Þ'=> 'TH', 'Ÿ'=> 'Y', 'ß'=> 'ss', 'à'=> 'a',
            'á'=> 'a', 'â'=> 'a', 'ã'=> 'a', 'ä'=> 'a', 'å'=> 'a', 'æ'=> 'ae', 'ç'=> 'c',
            'è'=> 'e', 'é'=> 'e', 'ê'=> 'e', 'ë'=> 'e', 'ì'=> 'i', 'í'=> 'i', 'î'=> 'i',
            'ï'=> 'i', 'ð'=> 'd', 'ñ'=> 'n', 'ò'=> 'o', 'ó'=> 'o', 'ô'=> 'o', 'õ'=> 'o',
            'ö'=> 'o', 'ő'=> 'o', 'ø'=> 'o', 'ù'=> 'u', 'ú'=> 'u', 'û'=> 'u', 'ü'=> 'u',
            'ű'=> 'u', 'ý'=> 'y', 'þ'=> 'th', 'ÿ'=> 'y',
            //Greek
            'α'=> 'a', 'β'=> 'b', 'γ'=> 'g', 'δ'=> 'd', 'ε'=> 'e', 'ζ'=> 'z', 'η'=> 'h',
            'θ'=> '8', 'ι'=> 'i', 'κ'=> 'k', 'λ'=> 'l', 'μ'=> 'm', 'ν'=> 'n', 'ξ'=> '3',
            'ο'=> 'o', 'π'=> 'p', 'ρ'=> 'r', 'σ'=> 's', 'τ'=> 't', 'υ'=> 'y', 'φ'=> 'f',
            'χ'=> 'x', 'ψ'=> 'ps', 'ω'=> 'w', 'ά'=> 'a', 'έ'=> 'e', 'ί'=> 'i', 'ό'=> 'o',
            'ύ'=> 'y', 'ή'=> 'h', 'ώ'=> 'w', 'ς'=> 's', 'ϊ'=> 'i', 'ΰ'=> 'y', 'ϋ'=> 'y',
            'ΐ'=> 'i', 'Α'=> 'A', 'Β'=> 'B', 'Γ'=> 'G', 'Δ'=> 'D', 'Ε'=> 'E', 'Ζ'=> 'Z',
            'Η'=> 'H', 'Θ'=> '8', 'Ι'=> 'I', 'Κ'=> 'K', 'Λ'=> 'L', 'Μ'=> 'M', 'Ν'=> 'N',
            'Ξ'=> '3', 'Ο'=> 'O', 'Π'=> 'P', 'Ρ'=> 'R', 'Σ'=> 'S', 'Τ'=> 'T', 'Υ'=> 'Y',
            'Φ'=> 'F', 'Χ'=> 'X', 'Ψ'=> 'PS', 'Ω'=> 'W', 'Ά'=> 'A', 'Έ'=> 'E', 'Ί'=> 'I',
            'Ό'=> 'O', 'Ύ'=> 'Y', 'Ή'=> 'H', 'Ώ'=> 'W', 'Ϊ'=> 'I', 'Ϋ'=> 'Y',
            //Romanian
            'ă'=> 'a', 'ș'=> 's', 'ț'=> 't',
            'Ă'=> 'A', 'Ș'=> 'S', 'Ț'=> 'T',
            //Russian
            'а'=> 'a', 'б'=> 'b', 'в'=> 'v', 'г'=> 'g', 'д'=> 'd', 'е'=> 'e', 'ё'=> 'yo',
            'ж'=> 'zh', 'з'=> 'z', 'и'=> 'i', 'й'=> 'j', 'к'=> 'k', 'л'=> 'l', 'м'=> 'm',
            'н'=> 'n', 'о'=> 'o', 'п'=> 'p', 'р'=> 'r', 'с'=> 's', 'т'=> 't', 'у'=> 'u',
            'ф'=> 'f', 'х'=> 'h', 'ц'=> 'c', 'ч'=> 'ch', 'ш'=> 'sh', 'щ'=> 'sh', 'ъ'=> '',
            'ы'=> 'y', 'ь'=> '', 'э'=> 'e', 'ю'=> 'yu', 'я'=> 'ya',
            'А'=> 'A', 'Б'=> 'B', 'В'=> 'V', 'Г'=> 'G', 'Д'=> 'D', 'Е'=> 'E', 'Ё'=> 'Yo',
            'Ж'=> 'Zh', 'З'=> 'Z', 'И'=> 'I', 'Й'=> 'J', 'К'=> 'K', 'Л'=> 'L', 'М'=> 'M',
            'Н'=> 'N', 'О'=> 'O', 'П'=> 'P', 'Р'=> 'R', 'С'=> 'S', 'Т'=> 'T', 'У'=> 'U',
            'Ф'=> 'F', 'Х'=> 'H', 'Ц'=> 'C', 'Ч'=> 'Ch', 'Ш'=> 'Sh', 'Щ'=> 'Sh', 'Ъ'=> '',
            'Ы'=> 'Y', 'Ь'=> '', 'Э'=> 'E', 'Ю'=> 'Yu', 'Я'=> 'Ya',
            //Ukrainian
            'Є'=> 'Ye', 'І'=> 'I', 'Ї'=> 'Yi', 'Ґ'=> 'G', 'є'=> 'ye', 'і'=> 'i',
            'ї'=> 'yi', 'ґ'=> 'g',
            //Czech
            'ě'=> 'e', 'ř'=> 'r',
            'ů'=> 'u', 'Ě'=> 'E', 'Ř'=> 'R',
            'Ů'=> 'U',
            //Slovak
            'ď'=> 'd', 'ľ'=> 'l',
            'ĺ'=> 'l', 'ň'=> 'n', 'ŕ'=> 'r', 'ť'=> 't',
            'Ď'=> 'D', 'Ľ'=> 'L',
            'Ĺ'=> 'L', 'Ň'=> 'N', 'Ŕ'=> 'R', 'Ť'=> 'T',
            //Polish
            'ć'=> 'c', 'ł'=> 'l', 'ń'=> 'n', 'ś'=> 's',
            'ź'=> 'z', 'ż'=> 'z',
            'Ć'=> 'C', 'Ł'=> 'L', 'Ń'=> 'N', 'Ś'=> 'S',
            'Ź'=> 'Z', 'Ż'=> 'Z',
            //Latvian
            'ā'=> 'a', 'ē'=> 'e', 'ģ'=> 'g', 'ī'=> 'i', 'ķ'=> 'k', 'ļ'=> 'l',
            'ņ'=> 'n',
            'Ā'=> 'A', 'Ē'=> 'E', 'Ģ'=> 'G', 'Ī'=> 'I', 'Ķ'=> 'K', 'Ļ'=> 'L',
            'Ņ'=> 'N',
            //Arabic
            'أ'=> 'a', 'ب'=> 'b', 'ت'=> 't', 'ث'=> 'th', 'ج'=> 'g', 'ح'=> 'h', 'خ'=> 'kh', 'د'=> 'd',
            'ذ'=> 'th', 'ر'=> 'r', 'ز'=> 'z', 'س'=> 's', 'ش'=> 'sh', 'ص'=> 's', 'ض'=> 'd', 'ط'=> 't',
            'ظ'=> 'th', 'ع'=> 'aa', 'غ'=> 'gh', 'ف'=> 'f', 'ق'=> 'k', 'ك'=> 'k', 'ل'=> 'l', 'م'=> 'm',
            'ن'=> 'n', 'ه'=> 'h', 'و'=> 'o', 'ي'=> 'y',
            //Lithuanian
            'ą'=> 'a', 'č'=> 'c', 'ę'=> 'e', 'ė'=> 'e', 'į'=> 'i', 'š'=> 's', 'ų'=> 'u',
            'ū'=> 'u', 'ž'=> 'z',
            'Ą'=> 'A', 'Č'=> 'C', 'Ę'=> 'E', 'Ė'=> 'E', 'Į'=> 'I', 'Š'=> 'S', 'Ų'=> 'U',
            'Ū'=> 'U', 'Ž'=> 'Z',
            //Serbian
            'ђ'=> 'dj', 'ј'=> 'j', 'љ'=> 'lj', 'њ'=> 'nj', 'ћ'=> 'c', 'џ'=> 'dz',
            'đ'=> 'dj', 'Ђ'=> 'Dj', 'Ј'=> 'j', 'Љ'=> 'Lj', 'Њ'=> 'Nj', 'Ћ'=> 'C',
            'Џ'=> 'Dz', 'Đ'=> 'Dj',
            //Azerbajani
            'ə'=> 'e', 'ğ'=> 'g', 'ı'=> 'i', 'ş'=> 's',
            'Ə'=> 'E', 'Ğ'=> 'G', 'İ'=> 'I', 'Ş'=> 'S',
            //Georgian
            'ა'=> 'a', 'ბ'=> 'b', 'გ'=> 'g', 'დ'=> 'd', 'ე'=> 'e', 'ვ'=> 'v', 'ზ'=> 'z',
            'თ'=> 't', 'ი'=> 'i', 'კ'=> 'k', 'ლ'=> 'l', 'მ'=> 'm', 'ნ'=> 'n', 'ო'=> 'o',
            'პ'=> 'p', 'ჟ'=> 'j', 'რ'=> 'r', 'ს'=> 's', 'ტ'=> 't', 'უ'=> 'u', 'ფ'=> 'f',
            'ქ'=> 'q', 'ღ'=> 'g', 'ყ'=> 'y', 'შ'=> 'sh', 'ჩ'=> 'ch', 'ც'=> 'c', 'ძ'=> 'dz',
            'წ'=> 'w', 'ჭ'=> 'ch', 'ხ'=> 'x', 'ჯ'=> 'j', 'ჰ'=> 'h'
        ];
        $result = str_replace(array_keys($transliterateChars), array_values($transliterateChars), $string);
        $result = preg_replace("/[^-\w\s]/", '', $result);
        $result = preg_replace("/[-\s]+/", '', $result);
        $result = preg_replace("/-+$/", '', $result);
        $result = strtolower($result);
        return $result;
    }


}
