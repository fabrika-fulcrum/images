<?php
namespace Fulcrum\Images\Analysis;

use ColorThief\ColorThief;
use Fulcrum\Images\Geometry\Color;
use Imagick;

class ExtractColors {

    /**
     * @param \Imagick $sourceImg
     * @param int $size
     * @return array
     */
    public function extractColors(Imagick $sourceImg, $size = 0) {
        $sourceImg = clone ($sourceImg);

        if ($size > 0) {
            $width = $sourceImg->getImageWidth();
            $height = $sourceImg->getImageHeight();
            $sourceImg = clone($sourceImg);
            if ($height > $width) {
                $o_width = $size;
                $o_height = $o_width * $height / $width;
            } else {
                $o_height = $size;
                $o_width = $o_height * $width / $height;
            }
            $sourceImg->resizeImage($o_width, $o_height, Imagick::FILTER_CUBIC, 1);
        }

        $result = [];
        $colors = [];
        try {
            $rgbPalette = ColorThief::getPalette($sourceImg, 10);

            foreach ($rgbPalette as $rgbTriad) {
                $colors[] = new Color($rgbTriad[0] / 255, $rgbTriad[1] / 255, $rgbTriad[2] / 255);
            }

            $result['main'] = $colors[0];
            $result['palette'] = $colors;

            $maxDist = 0;
            $highlight = null;
            foreach ($colors as $color) {
                $dist = $result['main']->distanceFrom($color);
                if ($dist > $maxDist) {
                    $maxDist = $dist;
                    $highlight = $color;
                }
            }

            $result['highlight'] = $highlight;
        } catch (\Exception $e) {

        }
        return $result;
    }
}
