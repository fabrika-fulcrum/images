<?php
namespace Fulcrum\Images\Analysis;

use Fulcrum\Images\Analysis\FaceDetector\FaceDetector;

class DetectFaces {
   
    public function performDetection($image) {
        $detector = new FaceDetector();
        $detector->scan($image->getSource());
        $faces = $detector->getFaces();
        return $faces;
    }
}
