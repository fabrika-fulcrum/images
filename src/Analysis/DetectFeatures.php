<?php
namespace Fulcrum\Images\Analysis;

use Imagick;

class DetectFeatures {
   
    /**
     * @param \Imagick $sourceImg
     * @param int $size
     * @param int $blur
     * @return array
     */
    public function calculateFeaturesMap(Imagick $image, $size=50, $blur = 5) {

        $width = $image->getImageWidth();
        $height = $image->getImageHeight();

        if ($height > $width) {
            $o_width = $size * 10;
            $o_height = $o_width * $height / $width;
        } else {
            $o_height = $size * 10;
            $o_width = $o_height * $width / $height;
        }
        $min_width = floor($o_width / 10);
        $min_height = floor($o_height / 10);

        // Details layer

        /*$detailImg = clone ($image);
        $bias = 1;
        $detailImg->resizeImage($o_width, $o_height, Imagick::FILTER_CUBIC, 1);
        //$detailImg->setImageBias($bias * \Imagick::getQuantum());
        $edgeFindingKernel = [-1, -1, -1, -1, 8, -1, -1, -1, -1];
        //$detailImg->convolveImage($edgeFindingKernel);
        $detailImg->edgeImage(1);
        header("Content-Type: image/jpg");
        echo $detailImg->getImageBlob();
        die();*/

        $detailImg = clone ($image);
        $detailImg->resizeImage($o_width, $o_height, Imagick::FILTER_LANCZOS, 1);
        $detailBlurImg = clone($detailImg);
        $detailBlurImg->gaussianBlurImage($blur*2, $blur*2);
        $detailBlurImg->negateImage(0);
        $detailBlurImg->setOption('compose:args', '50');
        $detailImg->compositeImage($detailBlurImg, Imagick::COMPOSITE_BLEND, 0, 0);
        $detailImg->negateImage(0);
        $detailImg->modulateImage(100, 0, 0);
        $detailImg->normalizeImage();
        $detailImg->resizeImage($min_width, $min_height, Imagick::FILTER_BOX, 1);
        $detailImg->normalizeImage();

        $details = [];
        for ($x = 0; $x < $min_width; $x++) {
            $details[$x] = [];
            for ($y = 0; $y < $min_height; $y++) {
                $px = $detailImg->getImagePixelColor($x, $y)->getHSL();
                $details[$x][$y] = round($px['luminosity']*255);
            }
        }

        // Deviation Image;

        $diffImg = clone ($image);

        $refPixel = clone ($image);
        $refPixel->resizeImage($width/4, $height/4, Imagick::FILTER_CUBIC, 1);
        $refPixel->resizeImage($width/40, $height/40, Imagick::FILTER_CUBIC, 1);
        $refPixel->resizeImage(1, 1, Imagick::FILTER_CUBIC, 1);
        $refPixel->resizeImage($min_width, $min_height, Imagick::FILTER_BOX,1);
        $diffImg->resizeImage($min_width, $min_height, Imagick::FILTER_BOX, 1);
        $diffImg->compositeImage($refPixel, Imagick::COMPOSITE_DIFFERENCE, 0, 0);

        $saturation = [];
        $luminance = [];
        for ($x = 0; $x < $min_width; $x++) {
            $saturation[$x] = [];
            $luminance[$x] = [];
            for ($y = 0; $y < $min_height; $y++) {
                $px = $diffImg->getImagePixelColor($x, $y)->getHSL();
                $saturation[$x][$y] = round($px['saturation']*255);
                $luminance[$x][$y] = round($px['luminosity']*255);
            }
        }

        return [
            "details"=>$details,
            "luminance"=>$luminance,
            "saturation"=>$saturation
        ];
    }
}
