<?php
namespace Fulcrum\Images\Analysis;

use Fulcrum\Images\Geometry\Box;
use Fulcrum\Images\Geometry\Color;
use Fulcrum\Images\Geometry\Point;
use Imagick;

class DetectBorder {
    protected $tolerance = 20;

    /**
     * @var Imagick
     */
    protected $resizedImage;

    /**
     * @var Box
     */
    protected $size;
    protected $pixels = [];

    public function analyzeBorder(Imagick $sourceImg, $size = 0) {
        $width = $sourceImg->getImageWidth();
        $height = $sourceImg->getImageHeight();

        $this->resizedImage = clone($sourceImg);

        if ($size > 0) {
            if ($height > $width) {
                $o_width = $size;
                $o_height = $o_width * $height / $width;
            } else {
                $o_height = $size;
                $o_width = $o_height * $width / $height;
            }
            $this->resizedImage->resizeImage($o_width, $o_height, Imagick::FILTER_CUBIC, 1);
        }

        $this->size = Box::Create($this->resizedImage->getImageWidth(),$this->resizedImage->getImageHeight());
        $borderPixelsCount = ($this->size->getWidth() * 2 + $this->size->getHeight() * 2) - 4;

        for ($i=0; $i<=$borderPixelsCount; $i++) {
            $point = $this->indexToPoint($i);
            $px = $this->resizedImage->getImagePixelColor($point->getX(), $point->getY());
            $this->pixels[$i] = Color::CreateFromImagickPixel($px);
        }

        $histogram = [];
        $freqMaxValue = 0;
        foreach ($this->pixels as $pixel) {
            $key = $pixel->toHexRGB();
            if (array_key_exists($key,$histogram)) {
                $histogram[$key]++;
            } else {
                $histogram[$key] = 1;
            }
            if ($histogram[$key] > $freqMaxValue) {
                $freqMaxValue = $histogram[$key];
            }
        }
        asort($histogram);
        $histogram = array_reverse($histogram);

        //analyze distance and spread

        $threshold_similarity = 0.25;
        $threshold_percentage = 0.75;

        //----------------------------

        $colors = array_keys($histogram);
        $weights = array_values($histogram);
        for ($i = 0; $i<count($colors); $i++) {
            $colors[$i] = Color::CreateFromHex($colors[$i]);
        }

        $largestWeight = 0;
        $largestGroup = [];


        for ($i = 0; $i< count($histogram); $i++ ){
            //_d($i,'color index');
            $currentWeight = 0;
            $currentGroup = [];

            /** @var Color $currentColor */
            $currentColor = $colors[$i];

            for ($j = $i; $j >= 0; $j--) {
                $dist = $currentColor->distanceFrom($colors[$j]);
                if ($dist <= $threshold_similarity) {
                    $currentWeight+=$weights[$j];
                    $currentGroup[] = [
                        'color' => $colors[$j],
                        'count' => $weights[$j]
                    ];
                } else {
                    break;
                }
            }

            for ($k = $i+1; $k < count($colors)-$i; $k++) {
                $dist = $currentColor->distanceFrom($colors[$k]);
                if ($dist <= $threshold_similarity) {
                    $currentWeight+=$weights[$k];
                    $currentGroup[] = [
                        'color' => $colors[$k],
                        'count' => $weights[$k]
                    ];
                } else {
                    break;
                }
            }
            if ($currentWeight > $largestWeight) {
                $largestWeight = $currentWeight;
                $largestGroup = $currentGroup;
            }
        }

        if ($borderPixelsCount * $threshold_percentage > $largestWeight) {
            return false;
        }

        $sum = 0;
        $averages = [
            'r'=>0,
            'g'=>0,
            'b'=>0,
            'a'=>0
        ];

        foreach ($largestGroup as $pickedColor) {
            $sum += $pickedColor['count'];
            $averages['r']+=$pickedColor['count'] * $pickedColor['color']->red();
            $averages['g']+=$pickedColor['count'] * $pickedColor['color']->green();
            $averages['b']+=$pickedColor['count'] * $pickedColor['color']->blue();
            $averages['a']+=$pickedColor['count'] * $pickedColor['color']->alpha();
        }

        $result = new Color(
            $averages['r']/$sum,
            $averages['g']/$sum,
            $averages['b']/$sum,
            $averages['a']/$sum
        );

        return $result;

    }

    /**
     * @param int $index
     * @return Point
     */
    protected function indexToPoint($index) {
        $w = $this->size->getWidth();
        $h = $this->size->getHeight();

        if ($index < $w) {
            return Point::Create($index, 0);
        }
        if ($index >= $w && $index < $w+$h-1) {
            return Point::Create($w-1,$index-$w+1);
        }
        if ($index >= $w+$h - 3 && $index < $w * 2 + $h - 3) {
            return Point::Create($w-($index-($w+$h-3)), $h-1);
        }
        return Point::Create(0,($w*2+$h*2-4)-$index);
    }
}
